class DataLayer():
    def __init__(self, driver):
        self.driver = driver

    def get_data_layer(self):
        return self._run('''return window.dataLayer;''')

    def get_data_layer_after_event(self, event_name):
        event_index = self._find_event(event_name)
        if event_index == -1:
            return None
        return self._run('return window.dataLayer[parseInt(%s, 10)];' % event_index)

    def get_payload_for_event(self, event_name):
        event = self.get_data_layer_after_event(event_name)
        return event['payload'] if event else None

    def push_to_data_layer(self, value):
        return self._run('window.dataLayer.push({});'.format(value))

    def _run(self, script):
        return self.driver.execute_script(script)

    def _find_event(self, event_name):
        return self._run('return window.dataLayer.findIndex(function(event) { return event.event === "%s"; });'
                          % event_name)



