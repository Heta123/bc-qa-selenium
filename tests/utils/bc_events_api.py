import requests
import json
import datetime
from datetime import datetime, timedelta
from mimesis import Text


class Error(Exception):
    """Class for error"""
    pass


class APIRequestError(Error):
    """Api request invalid"""


class BCEventsAPI:
    def __init__(self, base_url_events):
        removed_scheme = base_url_events.replace('http://', '')
        split_url = removed_scheme.split('.', 1)
        env = split_url[1]
        library = split_url[0]
        self.endpoint = 'http://{}.{}'.format(library, env)


class BCEventsAPIEvents(BCEventsAPI):
    def __init__(self, base_url_events):
        BCEventsAPI.__init__(self, base_url_events)

    def insert_event_series(self, type_of_event, sub_domain, title="Py-Insert event series", recurrences=5,
                            pattern_length=10, relative_start=5, relative_end=1, relative_type_start="days",
                            relative_type_end="days"):
        description_text = Text('en').text(quantity=3)
        date_now = datetime.today()
        locations = BCEventsAPILocations.get_location(self)
        types = BCEventsAPIType.get_types(self)
        audience = BCEventsAPIAudiences.get_audiences(self)
        languages = BCEventsAPILanguages.get_languages(self)

        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
        if type_of_event == "single_event":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=3)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " single_event " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[1]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": None,
                                "enabled_methods": [],
                                "registration_start": {},
                                "registration_end": {},
                                "is_full": False,
                                "cap": None,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": None,
                                "ordinal": [
                                    "0"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": []
                                },
                                "expire_count": None
                            }
                        ]
                    }
                })
        elif type_of_event == "multiday":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=72)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " multiday " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": None,
                                "enabled_methods": [],
                                "registration_start": {},
                                "registration_end": {},
                                "is_full": False,
                                "cap": None,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": None,
                                "ordinal": [
                                    "0"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": []
                                },
                                "expire_count": None
                            }
                        ]
                    }
                })
        elif type_of_event == "daily_event":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            expire_date = date_now + timedelta(days=pattern_length)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            expire_date_modified = expire_date.strftime("%Y-%m-%d")
            title = title + " daily_event " + start_date

            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": None,
                                "enabled_methods": [],
                                "registration_start": {},
                                "registration_end": {},
                                "is_full": False,
                                "cap": None,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "daily",
                                "ordinal": [
                                    "1"
                                ],
                                "expire_date": expire_date_modified,
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": []
                                },
                                "expire_count": None
                            }
                        ]
                    }
                })
        elif type_of_event == "weekly_event_recurrences":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " weekly_event_recurrences " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": None,
                                "enabled_methods": [],
                                "registration_start": {},
                                "registration_end": {},
                                "is_full": False,
                                "cap": None,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekday",
                                "ordinal": [
                                    "1"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })
        elif type_of_event == "monthly_event_recurrences":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " monthly_event_recurrences" + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": None,
                                "enabled_methods": [],
                                "registration_start": {},
                                "registration_end": {},
                                "is_full": False,
                                "cap": None,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekdayPerMonth",
                                "ordinal": [
                                    "3"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })
        elif type_of_event == "single_event_with_registration":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=3)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " single_event_with_registration " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[1]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "registration_start": {
                                    "window_type": "RELATIVE",
                                    "ordinal": 5,
                                    "unit": 'days'
                                },
                                "registration_end": {
                                    "window_type": 'RELATIVE',
                                    "ordinal": 1,
                                    "unit": "days"
                                },
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": None,
                                "ordinal": [
                                    "0"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": []
                                },
                                "expire_count": None
                            }
                        ]
                    }
                })
        elif type_of_event == "daily_event_with_registration":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            expire_date = date_now + timedelta(days=pattern_length)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            expire_date_modified = expire_date.strftime("%Y-%m-%d")
            title = title + " daily_event_with_registration " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": 'days'},
                                "registration_end": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": "days"
                                },
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "daily",
                                "ordinal": [
                                    "1"
                                ],
                                "expire_date": expire_date_modified,
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": []
                                },
                                "expire_count": None
                            }
                        ]
                    }
                })
        elif type_of_event == "weekly_event_with_registration":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " weekly_event_with_registration " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": "RELATIVE",
                                    "ordinal": 5,
                                    "unit": 'days'},
                                "registration_end": {
                                    "window_type": 'RELATIVE',
                                    "ordinal": 1,
                                    "unit": "days"},
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekdayPerMonth",
                                "ordinal": [
                                    "3"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })
        elif type_of_event == "monthly_event_recurrences_with_registration":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " monthly_event_recurrences_with_registration " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": "RELATIVE",
                                    "ordinal": 5,
                                    "unit": 'days'
                                },
                                "registration_end": {
                                    "window_type": 'RELATIVE',
                                    "ordinal": 1,
                                    "unit": "days"
                                },
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekdayPerMonth",
                                "ordinal": [
                                    "3"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })
        elif type_of_event == "daily_event_with_registration_to_all":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            expire_date = date_now + timedelta(days=pattern_length)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            expire_date_modified = expire_date.strftime("%Y-%m-%d")
            title = title + " daily_event_with_registration_to_all " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": 'days'},
                                "registration_end": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": "days"
                                },
                                "scope": "ALL_REMAINING",
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "daily",
                                "ordinal": [
                                    "1"
                                ],
                                "expire_date": expire_date_modified,
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": []
                                },
                                "expire_count": None
                            }
                        ]
                    }
                })
        elif type_of_event == "weekly_event_with_registration_to_all":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " weekly_event_with_registration_to_all " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": 'days'
                                },
                                "registration_end": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": "days"
                                },
                                "scope": "ALL_REMAINING",
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekdayPerMonth",
                                "ordinal": [
                                    "3"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })
        elif type_of_event == "monthly_event_recurrences_with_registration_to_all":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " monthly_event_recurrences_with_registration_to_all " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": 'days'
                                },
                                "registration_end": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": "days"
                                },
                                "scope": "ALL_REMAINING",
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekdayPerMonth",
                                "ordinal": [
                                    "3"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })
        elif type_of_event == "daily_event_with_registration_to_all_and_ends_at_last_event":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            expire_date = date_now + timedelta(days=pattern_length)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            expire_date_modified = expire_date.strftime("%Y-%m-%d")
            title = title + " daily_event_with_registration_to_all_and_ends_at_last_event " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": 'days'},
                                "registration_end": {
                                    "window_type": "EVENT_END",
                                    "ordinal": 1,
                                    "unit": "days"
                                },
                                "scope": "ALL_REMAINING",
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "daily",
                                "ordinal": [
                                    "1"
                                ],
                                "expire_date": expire_date_modified,
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": []
                                },
                                "expire_count": None
                            }
                        ]
                    }
                })
        elif type_of_event == "weekly_event_with_registration_to_all_and_ends_at_last_event":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " weekly_event_with_registration_to_all_and_ends_at_last_event " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": 'days'
                                },
                                "registration_end": {
                                    "window_type": '"EVENT_END"',
                                    "ordinal": 1,
                                    "unit": "days"
                                },
                                "scope": "ALL_REMAINING",
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekdayPerMonth",
                                "ordinal": [
                                    "3"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })
        elif type_of_event == "monthly_event_recurrences_with_registration_to_all_and_ends_at_last_event":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " monthly_event_recurrences_with_registration_to_all_and_ends_at_last_event " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": None,
                                    "ordinal": 1,
                                    "unit": 'days'
                                },
                                "registration_end": {
                                    "window_type": "EVENT_END",
                                    "ordinal": 1,
                                    "unit": "days"
                                },
                                "scope": "ALL_REMAINING",
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekdayPerMonth",
                                "ordinal": [
                                    "3"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })
        elif type_of_event == "single_event_with_registration_starts_before_and_closes":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=3)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " single_event_with_registration_starts_before_and_closes " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[1]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "registration_start": {
                                    "window_type": "RELATIVE",
                                    "ordinal": relative_start,
                                    "unit": relative_type_start
                                },
                                "registration_end": {
                                    "window_type": "RELATIVE",
                                    "ordinal": relative_end,
                                    "unit": relative_type_end
                                },
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": None,
                                "ordinal": [
                                    "0"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": []
                                },
                                "expire_count": None
                            }
                        ]
                    }
                })
        elif type_of_event == "daily_event_with_registration_starts_before_and_closes":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            expire_date = date_now + timedelta(days=pattern_length)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            expire_date_modified = expire_date.strftime("%Y-%m-%d")
            title = title + " daily_event_with_registration_starts_before_and_closes " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": "RELATIVE",
                                    "ordinal": relative_start,
                                    "unit": relative_type_start
                                },
                                "registration_end": {
                                    "window_type": "RELATIVE",
                                    "ordinal": relative_end,
                                    "unit": relative_type_end
                                },
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "daily",
                                "ordinal": [
                                    "1"
                                ],
                                "expire_date": expire_date_modified,
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": []
                                },
                                "expire_count": None
                            }
                        ]
                    }
                })
        elif type_of_event == "weekly_event_with_registration_starts_before_and_closes":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " weekly_event_with_registration_starts_before_and_closes " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": "RELATIVE",
                                    "ordinal": relative_start,
                                    "unit": relative_type_start
                                },
                                "registration_end": {
                                    "window_type": "RELATIVE",
                                    "ordinal": relative_end,
                                    "unit": relative_type_end
                                },
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekdayPerMonth",
                                "ordinal": [
                                    "3"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })
        elif type_of_event == "monthly_event_recurrences_with_registration_starts_before_and_closes":
            start_hours = date_now + timedelta(hours=2)
            end_hours = date_now + timedelta(hours=4)
            start_date = start_hours.strftime("%Y-%m-%dT%H:00")
            end_date = end_hours.strftime("%Y-%m-%dT%H:00")
            title = title + " monthly_event_recurrences_with_registration_starts_before_and_closes " + start_date
            payload = json.dumps(
                {
                    "event_series": {
                        "library_id": sub_domain,
                        "definition": {
                            "title": title,
                            "description": description_text,
                            "start": start_date,
                            "end": end_date,
                            "library_id": sub_domain,
                            "branch_location_id": locations[2],
                            "type_ids": [
                                types[10]
                            ],
                            "audience_ids": [
                                audience[1]
                            ],
                            "language_ids": [
                                languages[0]
                            ],
                            "creator_bc_id": "89898989",
                            "creator_name": "Barry Allen",
                            "registration_info": {
                                "provider": "BIBLIO_EVENTS",
                                "enabled_methods": [],
                                "registration_start": {
                                    "window_type": "RELATIVE",
                                    "ordinal": relative_start,
                                    "unit": relative_type_start
                                },
                                "registration_end": {
                                    "window_type": "RELATIVE",
                                    "ordinal": relative_end,
                                    "unit": relative_type_end
                                },
                                "is_full": False,
                                "cap": 10,
                                "max_seats": 10,
                                "instructions": description_text
                            },
                            "contact": {
                                "name": "Barry Allen"
                            }
                        },
                        "recurrence_patterns": [
                            {
                                "generator": "weekdayPerMonth",
                                "ordinal": [
                                    "3"
                                ],
                                "pattern_start": start_date,
                                "pattern_end": end_date,
                                "generator_args": {
                                    "weekday": ['monday']
                                },
                                "expire_count": recurrences
                            }
                        ]
                    }
                })

        response = requests.post(self.endpoint + "/events/event_series", data=payload, headers=headers)
        if response.status_code is not 201:
            print(response.status_code)
            print(response.json())

        event_id = response.json()['event_series']['id']
        return event_id

    def delete_event_series(self, event_id):
        headers = {
            "Accept": "application/json"
        }
        response = requests.delete(self.endpoint + "/events/event_series/{}".format(event_id), headers=headers)
        if response.status_code is not 204:
            print(response.status_code)
            print(response.json())

    def publish_event_series(self, event_id):
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json"

        }
        payload = json.dumps(
            {
                "workflow_action": {
                    "action_type": "publish"
                }
            }
        )
        response = requests.post(self.endpoint + "/events/event_series/{}/workflow_actions".format(event_id),
                                 data=payload, headers=headers)
        if response.status_code is not 201:
            print(response.status_code)
            print(response.json())

    def unpublish_event_series(self, event_id):
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json"

        }
        payload = json.dumps(
            {
                "workflow_action": {
                    "action_type": "unpublish"
                }
            }
        )
        response = requests.post(self.endpoint + "/events/event_series/{}/workflow_actions".format(event_id),
                                 data=payload, headers=headers)
        if response.status_code is not 201:
            print(response.status_code)
            print(response.json())

    def get_event_series_by_id(self, event_id):
        headers = {
            "Accept": "application/json"
        }

        response = requests.get(self.endpoint + "/events/event_series/{}".format(event_id), headers=headers)
        if response.status_code is not 200:
            print(response.status_code)
            print(response.json())

        event_title = response.json()['event_series']['definition']['title']
        return event_title


class BCEventsAPILocations(BCEventsAPI):
    def __init__(self, base_url):
        BCEventsAPI.__init__(self, base_url)

    def get_location(self):
        headers = {
            "Accept": "application/json"
        }
        response = requests.get(self.endpoint + "/locations/locations", headers=headers)

        if response.status_code is not 200:
            print(response.status_code)
            print(response.json())
            raise APIRequestError

        locations = response.json()['locations']
        ids = []
        for locations in locations:
            ids.append(locations['id'])
        return ids

    def get_location_by_id(self, location_id):
        headers = {
            "Accept": "application/json"
        }
        response = requests.get(self.endpoint + "/locations/locations/{}".format(location_id), headers=headers)
        if response.status_code is not 200:
            print(response.status_code)
            print(response.json())
            raise APIRequestError

        location_id = response.json()['location']
        return location_id


class BCEventsAPIAudiences(BCEventsAPI):
    def __init__(self, base_url):
        BCEventsAPI.__init__(self, base_url)

    def get_audiences(self):
        headers = {
            "Accept": "application/json"
        }
        response = requests.get(self.endpoint + "/events/event_audiences?limit=0", headers=headers)
        if response.status_code is not 200:
            print(response.status_code)
            print(response.json())
            raise APIRequestError

        audiences = response.json()['event_audiences']
        ids = []
        for audiences in audiences:
            ids.append(audiences['id'])
        return ids


class BCEventsAPIPrograms(BCEventsAPI):
    def __init__(self, base_url):
        BCEventsAPI.__init__(self, base_url)

    def get_programs(self):
        headers = {
            "Accept": "application/json"
        }
        response = requests.get(self.endpoint + "/events/event_programs", headers=headers)
        if response.status_code is not 200:
            print(response.status_code)
            print(response.json())
            raise APIRequestError

        programs = response.json()['event_programs']
        ids = []
        for programs in programs:
            ids.append(programs['id'])
        return ids


class BCEventsAPIType(BCEventsAPI):
    def __init__(self, base_url):
        BCEventsAPI.__init__(self, base_url)

    def get_types(self):
        headers = {
            "Accept": "application/json"
        }
        response = requests.get(self.endpoint + "/events/event_types?limit=0", headers=headers)
        if response.status_code is not 200:
            print(response.status_code)
            print(response.json())
            raise APIRequestError

        types = response.json()['event_types']
        ids = []
        for types in types:
            ids.append(types['id'])
        return ids


class BCEventsAPILanguages(BCEventsAPI):
    def __init__(self, base_url):
        BCEventsAPI.__init__(self, base_url)

    def get_languages(self):
        headers = {
            "Accept": "application/json"
        }
        response = requests.get(self.endpoint + "/events/event_languages", headers=headers)
        if response.status_code is not 200:
            print(response.status_code)
            print(response.json())
            raise APIRequestError

        languages = response.json()['event_languages']
        ids = []
        for language in languages:
            ids.append(language['id'])
        return ids
