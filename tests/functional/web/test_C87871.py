import pytest
import allure
import sure
import configuration.user
import configuration.system
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException, StaleElementReferenceException
from selenium.webdriver.support.ui import WebDriverWait
from mimesis import Text, Internet
from utils.image_download_helper import *
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.page_builder import PageBuilderPage
from pages.web.components.page_builder.page_builder_modules import SideBarMenu, RowModules, CatalogCarousel, TabbedCatalogCarousel, CommentsCarousel, QuotesCarousel, ModulesWithPlaceHolders
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_custom_page import CreateNewCustomPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.core.v2.search_results import SearchResultsPage
from utils.selenium_helpers import click, enter_text

PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CORE_BASE_URL = "https://chipublib-sandbox-local.stage.bibliocommons.com"
CUSTOM_CARD_INFO = {
    'title': ''.join(Text('en').words(quantity=3)),
    'url': Internet('en').home_page(),
    'description': ' '.join(Text('en').words(quantity=6))
}
SEARCH_TERM_ONE = Text('en').word()
SEARCH_TERM_TWO = Text('en').word()
ACTIVE_FILTER_ONE = "Book"


@pytest.mark.usefixtures('selenium_setup_and_teardown')
@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@allure.title("C87871: PB - Add carousels")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87871", "TestRail")
class TestC87871:
    def test_C87871(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as Implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page="bibliocommons-settings", tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        # Fetching search results from Core
        search_results_page_one = SearchResultsPage(self.driver, CORE_BASE_URL, query=SEARCH_TERM_ONE, search_type='keyword').open()
        page_one_url = self.driver.current_url
        try:
            search_results_page_one.filter_search_results(ACTIVE_FILTER_ONE).click()
            wait = WebDriverWait(self.driver, 20, poll_frequency=2, ignored_exceptions=[NoSuchElementException, IndexError, StaleElementReferenceException, ElementNotInteractableException])
            wait.until(lambda condition: search_results_page_one.search_filter_buttons[0].is_displayed())
            wait.until(lambda condition: ACTIVE_FILTER_ONE in search_results_page_one.search_filter_buttons[0].get_attribute("textContent"))
            page_one_url_with_active_filter = self.driver.current_url
        except NoSuchElementException:
            raise NoSuchElementException

        SearchResultsPage(self.driver, CORE_BASE_URL, query=SEARCH_TERM_TWO, search_type='keyword').open()
        page_two_url = self.driver.current_url

        # Logging as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        # Create a New Page
        create_new_custom_page = CreateNewCustomPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        create_new_custom_page.title.send_keys(PAGE_TITLE)
        create_new_custom_page.page_builder_section.click()
        click(create_new_custom_page.publish)
        create_new_custom_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening the new page in the frontend
        new_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())
        new_page.wpheader.page_builder.click()
        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.rows_tab.click()

        # Adding a two columns module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.two_columns, new_page.page_builder.body).perform()

        two_columns = RowModules(new_page)
        wait.until(lambda condition: two_columns.column_body(0).is_displayed())
        ActionChains(self.driver).move_to_element(two_columns.column_body(0)).click().perform()
        wait.until(lambda condition: two_columns.column_settings.styles_tab.is_displayed())
        two_columns.column_settings.styles_tab.click()
        two_columns.column_settings.styles_tab_contents.select_equalize_heights("Yes")
        two_columns.column_settings.save.click()

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a sidebar menu to the page from Page Builder
        new_page.builder_panel.scroll_to_bottom()
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.sidebar_menu, two_columns.column_body(0)).perform()

        sidebar_menu = SideBarMenu(new_page)
        sidebar_menu.content_tab.click()
        sidebar_menu.content_tab_contents.select_a_menu.click()
        sidebar_menu.content_tab_contents.sidebar_menu_results(0).click()
        sidebar_menu.save.click()
        wait.until(lambda condition: len(new_page.user_facing_modules.sidebar_menus) == 1)

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a Catalog Carousel to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.catalog_carousel, two_columns.column_body(1)).perform()

        catalog_carousel = CatalogCarousel(new_page)
        wait.until(lambda condition: catalog_carousel.content_tab_contents.module_heading.is_displayed())
        catalog_carousel.content_tab.click()
        catalog_carousel.save()
        wait.until(lambda condition: catalog_carousel.is_placeholder_displayed)

        wait.until(lambda condition: new_page.page_builder.done.is_displayed())
        new_page.page_builder.done.click()
        wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()

        wait.until(lambda condition: catalog_carousel.is_placeholder_displayed)

        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())
        new_page.wpheader.page_builder.click()

        modules_with_placeholders = ModulesWithPlaceHolders(new_page)

        wait.until(lambda condition: modules_with_placeholders.module_body[0].is_displayed())
        ActionChains(self.driver).move_to_element(modules_with_placeholders.module_body[0]).click().perform()
        wait.until(lambda condition: catalog_carousel.content_tab_contents.module_heading.is_displayed())
        catalog_carousel.content_tab.click()
        enter_text(catalog_carousel.content_tab_contents.module_heading, "Catalog Carousel")
        enter_text(catalog_carousel.content_tab_contents.search_url, page_one_url)
        catalog_carousel.save()
        wait.until(lambda condition: len(new_page.user_facing_modules.catalog_carousels) == 1)
        wait.until(lambda condition: new_page.user_facing_modules.catalog_carousels[0].catalog_carousel_heading.is_displayed())

        wait.until(lambda condition: new_page.page_builder.done.is_displayed())
        new_page.page_builder.done.click()
        wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()
        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())

        new_page.open()

        wait.until(lambda condition: len(new_page.user_facing_modules.catalog_carousels) == 1)
        wait.until(lambda condition: new_page.user_facing_modules.catalog_carousels[0].catalog_carousel_heading.is_displayed())

        # Click the heading link and assert that it goes the the expected CORE page
        wait.until(lambda condition: new_page.user_facing_modules.catalog_carousels[0].carousel_heading_link(0).is_displayed())
        click(new_page.user_facing_modules.catalog_carousels[0].carousel_heading_link(0))
        wait.until(lambda condition: page_one_url in self.driver.current_url)

        new_page.open()

        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())
        new_page.wpheader.page_builder.click()

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a tabbed catalog carousel module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.tab_catalog_carousel, two_columns.column_body(1)).perform()

        tabbed_catalog_carousel = TabbedCatalogCarousel(new_page)
        wait.until(lambda condition: tabbed_catalog_carousel.content_tab_contents.module_heading.is_displayed())
        tabbed_catalog_carousel.content_tab.click()
        tabbed_catalog_carousel.save()
        wait.until(lambda condition: tabbed_catalog_carousel.is_placeholder_displayed)

        wait.until(lambda condition: new_page.page_builder.done.is_displayed())
        new_page.page_builder.done.click()
        wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()

        wait.until(lambda condition: tabbed_catalog_carousel.is_placeholder_displayed)

        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())
        new_page.wpheader.page_builder.click()

        wait.until(lambda condition: modules_with_placeholders.module_body[0].is_displayed())
        ActionChains(self.driver).move_to_element(modules_with_placeholders.module_body[0]).click().perform()
        wait.until(lambda condition: tabbed_catalog_carousel.content_tab_contents.module_heading.is_displayed())
        tabbed_catalog_carousel.content_tab.click()
        tabbed_catalog_carousel.content_tab_contents.module_heading.send_keys("Tabbed Catalog Carousel")
        click(tabbed_catalog_carousel.content_tab_contents.edit_carousel_tab(0))
        tabbed_catalog_carousel.content_tab_contents.edit_carousel_tab_contents.tab_label.send_keys("1")
        enter_text(tabbed_catalog_carousel.content_tab_contents.edit_carousel_tab_contents.search_url, page_one_url)
        tabbed_catalog_carousel.content_tab_contents.edit_carousel_tab_contents.save.click()
        click(tabbed_catalog_carousel.content_tab_contents.add_carousel_tab)
        wait.until(lambda condition: tabbed_catalog_carousel.content_tab_contents.edit_carousel_tab(1).is_displayed())
        click(tabbed_catalog_carousel.content_tab_contents.edit_carousel_tab(1))
        tabbed_catalog_carousel.content_tab_contents.edit_carousel_tab_contents.tab_label.send_keys("2")
        enter_text(tabbed_catalog_carousel.content_tab_contents.edit_carousel_tab_contents.search_url, page_two_url)
        tabbed_catalog_carousel.content_tab_contents.edit_carousel_tab_contents.save.click()
        tabbed_catalog_carousel.save()
        wait.until(lambda condition: len(new_page.user_facing_modules.catalog_carousels) == 2)
        wait.until(lambda condition: new_page.user_facing_modules.catalog_carousels[1].catalog_carousel_heading.is_displayed())

        wait.until(lambda condition: new_page.page_builder.done.is_displayed())
        new_page.page_builder.done.click()
        wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()
        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())

        new_page.open()

        wait.until(lambda condition: len(new_page.user_facing_modules.catalog_carousels) == 2)
        wait.until(lambda condition: new_page.user_facing_modules.catalog_carousels[1].catalog_carousel_heading.is_displayed())

        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())
        new_page.wpheader.page_builder.click()

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a comments carousel module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.comments_carousel, two_columns.column_body(1)).perform()

        comments_carousel = CommentsCarousel(new_page)
        wait.until(lambda condition: comments_carousel.content_tab_contents.module_heading.is_displayed())
        comments_carousel.content_tab.click()
        comments_carousel.save()
        wait.until(lambda condition: comments_carousel.is_placeholder_displayed)

        wait.until(lambda condition: new_page.page_builder.done.is_displayed())
        new_page.page_builder.done.click()
        wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()

        wait.until(lambda condition: comments_carousel.is_placeholder_displayed)

        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())
        new_page.wpheader.page_builder.click()

        wait.until(lambda condition: modules_with_placeholders.module_body[0].is_displayed())
        ActionChains(self.driver).move_to_element(modules_with_placeholders.module_body[0]).click().perform()
        wait.until(lambda condition: comments_carousel.content_tab_contents.module_heading.is_displayed())
        comments_carousel.content_tab.click()
        comments_carousel.content_tab_contents.module_heading.send_keys("Comments Carousel")
        comments_carousel.content_tab_contents.select_link_to("Custom URL")
        enter_text(comments_carousel.content_tab_contents.exit_link_url, page_one_url_with_active_filter)
        enter_text(comments_carousel.content_tab_contents.search_url, page_one_url_with_active_filter)
        comments_carousel.save()
        wait.until(lambda condition: len(new_page.user_facing_modules.comments_carousels) == 1)
        wait.until(lambda condition: new_page.user_facing_modules.comments_carousels[0].comments_carousel_heading.is_displayed())

        wait.until(lambda condition: new_page.page_builder.done.is_displayed())
        new_page.page_builder.done.click()
        wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()
        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())

        new_page.open()

        wait.until(lambda condition: len(new_page.user_facing_modules.comments_carousels) == 1)
        wait.until(lambda condition: new_page.user_facing_modules.comments_carousels[0].comments_carousel_heading.is_displayed())

        # Click the heading link and assert that it goes the the expected CORE page
        wait.until(lambda condition: new_page.user_facing_modules.comments_carousels[0].carousel_heading_link(0).is_displayed())
        click(new_page.user_facing_modules.comments_carousels[0].carousel_heading_link(0))
        wait.until(lambda condition: page_one_url_with_active_filter in self.driver.current_url)

        new_page.open()

        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())
        new_page.wpheader.page_builder.click()

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a quotes carousel module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.quotes_carousel, two_columns.column_body(1)).perform()

        quotes_carousel = QuotesCarousel(new_page)
        wait.until(lambda condition: quotes_carousel.content_tab_contents.module_heading.is_displayed())
        quotes_carousel.content_tab.click()
        quotes_carousel.save()
        wait.until(lambda condition: quotes_carousel.is_placeholder_displayed)

        wait.until(lambda condition: new_page.page_builder.done.is_displayed())
        new_page.page_builder.done.click()
        wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()

        wait.until(lambda condition: quotes_carousel.is_placeholder_displayed)

        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())
        new_page.wpheader.page_builder.click()

        wait.until(lambda condition: modules_with_placeholders.module_body[0].is_displayed())
        ActionChains(self.driver).move_to_element(modules_with_placeholders.module_body[0]).click().perform()
        wait.until(lambda condition: quotes_carousel.content_tab_contents.module_heading.is_displayed())
        quotes_carousel.content_tab.click()
        quotes_carousel.content_tab_contents.module_heading.send_keys("Quotes Carousel")
        quotes_carousel.content_tab_contents.select_link_to("Custom URL")
        enter_text(quotes_carousel.content_tab_contents.exit_link_url, page_one_url_with_active_filter)
        enter_text(quotes_carousel.content_tab_contents.search_url, page_one_url_with_active_filter)
        quotes_carousel.save()
        wait.until(lambda condition: len(new_page.user_facing_modules.quotes_carousels) == 1)
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_carousel_heading.is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(0).is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(1).is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(2).is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(3).is_displayed())

        wait.until(lambda condition: new_page.page_builder.done.is_displayed())
        new_page.page_builder.done.click()
        wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()
        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())

        ActionChains(self.driver).move_to_element(new_page.user_facing_modules.quotes_carousels[0].right_arrow).click().perform()

        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(4).is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(5).is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(6).is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(7).is_displayed())

        ActionChains(self.driver).move_to_element(new_page.user_facing_modules.quotes_carousels[0].left_arrow).click().perform()

        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(0).is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(1).is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(2).is_displayed())
        wait.until(lambda condition: new_page.user_facing_modules.quotes_carousels[0].quotes_cards(3).is_displayed())

        new_page.open()

        wait.until(lambda condition: len(new_page.user_facing_modules.quotes_carousels) == 1)
        wait.until(lambda condition: len(new_page.user_facing_modules.comments_carousels) == 1)
        wait.until(lambda condition: len(new_page.user_facing_modules.catalog_carousels) == 2)

        # Click the heading link and assert that it goes the the expected CORE page
        new_page.user_facing_modules.quotes_carousels[0].carousel_heading_link(0).click()
        wait.until(lambda condition: page_one_url_with_active_filter in self.driver.current_url)

        new_page.open()

        # Checking that Sidebar Menu height extends to the page height
        wait.until(lambda condition: new_page.wpheader.page_builder.is_displayed())
        new_page.wpheader.page_builder.click()
        two_columns.column_body(0).get_attribute("clientHeight").should.equal(two_columns.column_body(1).get_attribute("clientHeight"))

        # Deleting the created page
        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_input.send_keys(PAGE_TITLE)
        all_pages_page.search_button.click()
        all_pages_page.rows[0].title.get_attribute("textContent").should.match(PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        all_pages_page.rows[0].delete.click()
        all_pages_page.rows.should.be.empty
