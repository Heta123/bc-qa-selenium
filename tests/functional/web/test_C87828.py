import pytest
import allure
import sure
import configuration.user
import configuration.system
from utils.selenium_helpers import click
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.create_new_content.create_new_blog_post import CreateNewBlogPostPage
from pages.web.wpadmin.v3.create_new_content.create_new_news_post import CreateNewNewsPostPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_list_card import CreateNewListCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_catalog_comment_card import CreateNewCatalogCommentCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_event_card import CreateNewEventCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_online_resource_card import CreateNewOnlineResourceCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_poll_card import CreateNewPollCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_twitter_card import CreateNewTwitterCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_custom_card import CreateNewCustomCard
from pages.web.wpadmin.v3.create_new_featured_block.create_new_banner import CreateNewBannerPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_hero_slide import CreateNewHeroSlidePage
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage


PAGE = "bibliocommons-settings"

TAXONOMIES = ['Audience', 'Related Format', 'Programs and Campaigns', 'Genre', 'Topic', 'Tags']
taxonomies_terms = []


@pytest.fixture(scope='class')
def login_and_setup(request, selenium_setup_and_teardown):

    driver = request.cls.driver

    # Log in as Network Admin
    login_page = LoginPage(driver, configuration.system.base_url_web).open()
    login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                      configuration.user.user['web']['local']['admin']['password'])
    settings_system_settings_tab = SettingsSystemSettingsTabPage(driver, configuration.system.base_url_web,
                                                                 page=PAGE, tab='system').open()
    settings_system_settings_tab.v3_status_enabled.click()
    settings_system_settings_tab.save_changes.click()

    settings_general_tab = SettingsGeneralTabPage(driver, configuration.system.base_url_web, page=PAGE,
                                                  tab='generic').open()
    click(settings_general_tab.enable_make_tags_a_structured_taxonomy)
    settings_general_tab.save_changes.click()

    default_settings_page = DefaultSettingsPageBuilderPage(driver, configuration.system.base_url_web,
                                                           post_type='fl-builder-template',
                                                           page='default-page-builder-settings').open()
    default_settings_page.check_all_display_taxonomy_links_checkboxes()
    default_settings_page.save_changes_button_click()

    new_blog_post = CreateNewBlogPostPage(driver, configuration.system.base_url_web, post_type='post').open()

    # Creating a dynamic list of taxonomy terms, so that the test can be run on any environment
    for index, taxonomy in enumerate(TAXONOMIES):
        click(new_blog_post.taxonomy(taxonomy))
        taxonomies_terms.append(new_blog_post.taxonomy_terms(taxonomy)[0])
        if index == len(TAXONOMIES) - 1:
            break
        new_blog_post.taxonomies_evergreen_checkbox.click()

    driver.delete_all_cookies()

    # Logging in as lib admin
    login_page = LoginPage(driver, configuration.system.base_url_web).open()
    login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                      configuration.user.user['web']['local']['libadmin']['password'])


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('login_and_setup')
@allure.title("C87828: Structured Tags taxonomy")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87828", "TestRail")
class TestC87828:
    def test_C87828_1(self):
        new_blog_post = CreateNewBlogPostPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        for i in range(len(TAXONOMIES) - 1):
            new_blog_post.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_blog_post.is_tags_taxonomy_displayed.should.be.true
        new_blog_post.select_taxonomy_term("tags", taxonomies_terms[5])
        new_blog_post.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_2(self):
        new_news_post = CreateNewNewsPostPage(self.driver, configuration.system.base_url_web, post_type='bccms_news').open()

        for i in range(len(TAXONOMIES) - 1):
            new_news_post.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_news_post.is_tags_taxonomy_displayed.should.be.true
        new_news_post.select_taxonomy_term("tags", taxonomies_terms[5])
        new_news_post.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_3(self):
        new_list_card = CreateNewListCard(self.driver, configuration.system.base_url_web, post_type='bw_list').open()

        for i in range(len(TAXONOMIES) - 1):
            new_list_card.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_list_card.is_tags_taxonomy_displayed.should.be.true
        new_list_card.select_taxonomy_term("tags", taxonomies_terms[5])
        new_list_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_4(self):
        new_catalog_comment_card = CreateNewCatalogCommentCard(self.driver, configuration.system.base_url_web, post_type='bw_catalog_comment').open()

        for i in range(len(TAXONOMIES) - 1):
            new_catalog_comment_card.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_catalog_comment_card.is_tags_taxonomy_displayed.should.be.true
        new_catalog_comment_card.select_taxonomy_term("tags", taxonomies_terms[5])
        new_catalog_comment_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_5(self):
        new_event_card = CreateNewEventCard(self.driver, configuration.system.base_url_web, post_type='bw_event').open()

        for i in range(len(TAXONOMIES) - 1):
            new_event_card.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_event_card.is_tags_taxonomy_displayed.should.be.true
        new_event_card.select_taxonomy_term("tags", taxonomies_terms[5])
        new_event_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_6(self):
        new_online_resource_card = CreateNewOnlineResourceCard(self.driver, configuration.system.base_url_web, post_type='bw_or_card').open()

        for i in range(len(TAXONOMIES) - 1):
            new_online_resource_card.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_online_resource_card.is_tags_taxonomy_displayed.should.be.true
        new_online_resource_card.select_taxonomy_term("tags", taxonomies_terms[5])
        new_online_resource_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_7(self):
        new_poll_card = CreateNewPollCard(self.driver, configuration.system.base_url_web, post_type='bw_poll').open()

        for i in range(len(TAXONOMIES) - 1):
            new_poll_card.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_poll_card.is_tags_taxonomy_displayed.should.be.true
        new_poll_card.select_taxonomy_term("tags", taxonomies_terms[5])
        new_poll_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_8(self):
        new_twitter_card = CreateNewTwitterCard(self.driver, configuration.system.base_url_web, post_type='bw_twitter').open()

        for i in range(len(TAXONOMIES) - 1):
            new_twitter_card.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_twitter_card.is_tags_taxonomy_displayed.should.be.true
        new_twitter_card.select_taxonomy_term("tags", taxonomies_terms[5])
        new_twitter_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_9(self):
        new_custom_card = CreateNewCustomCard(self.driver, configuration.system.base_url_web, post_type='bw_custom_card').open()

        for i in range(len(TAXONOMIES) - 1):
            new_custom_card.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_custom_card.is_tags_taxonomy_displayed.should.be.true
        new_custom_card.select_taxonomy_term("tags", taxonomies_terms[5])
        new_custom_card.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_10(self):
        new_banner = CreateNewBannerPage(self.driver, configuration.system.base_url_web, post_type='bw_banner').open()

        for i in range(len(TAXONOMIES) - 1):
            new_banner.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_banner.is_tags_taxonomy_displayed.should.be.true
        new_banner.select_taxonomy_term("tags", taxonomies_terms[5])
        new_banner.is_tags_taxonomy_term_selected.should.be.true

    def test_C87828_11(self):
        new_hero_slide = CreateNewHeroSlidePage(self.driver, configuration.system.base_url_web, post_type='bw_hero_slide').open()

        for i in range(len(TAXONOMIES)):
            new_hero_slide.is_taxonomy_displayed(TAXONOMIES[i]).should.be.false
