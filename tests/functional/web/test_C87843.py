import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text, Internet
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_event_card import CreateNewEventCard
from utils.image_download_helper import *
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from utils.selenium_helpers import click
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage


PAGE = "bibliocommons-settings"
EVENT_CARD_INFO = {
    'title': ' '.join(Text('en').words(quantity=3)),
    'url': Internet('en').home_page(),
    'location': ' '.join(Text('en').words(quantity=3))
}
IMAGE_TITLE = '-'.join(Text('en').words(quantity=2))
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Event"


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87843: Create new card - Event Card (Manual)")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87843", "TestRail")
class TestC87843:
    def test_C87843(self):

        # Log in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web,
                                                                     page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        default_settings_page = DefaultSettingsPageBuilderPage(self.driver, configuration.system.base_url_web,
                                                               post_type='fl-builder-template',
                                                               page='default-page-builder-settings').open()
        default_settings_page.check_all_display_taxonomy_links_checkboxes()
        default_settings_page.save_changes_button_click()

        self.driver.delete_all_cookies()

        # Log in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        download_image("https://bit.ly/2EdPMqG", IMAGE_PATH)

        # Creating a new event card
        new_event_card = CreateNewEventCard(self.driver, configuration.system.base_url_web, post_type='bw_event').open()
        new_event_card.manually_enter_event_details.click()
        new_event_card.manual_event_fields.card_title.send_keys(EVENT_CARD_INFO['title'])
        new_event_card.manual_event_fields.event_url.send_keys(EVENT_CARD_INFO['url'])
        new_event_card.manual_event_fields.location.send_keys(EVENT_CARD_INFO['location'])
        new_event_card.scroll_to_top()

        click(new_event_card.manual_event_fields.card_image)
        new_event_card.manual_event_fields.select_widget_image.upload_files_tab.click()
        new_event_card.manual_event_fields.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_event_card.manual_event_fields.select_widget_image.add_image_to_widget_button.is_displayed())
        click(new_event_card.manual_event_fields.select_widget_image.add_image_to_widget_button)
        new_event_card.select_default_image_crop_views()

        new_event_card.is_free_text_tags_field_displayed.should.be.true
        num_of_taxonomies = 3
        for i in range(num_of_taxonomies):
            tag = Text('en').words(quantity=1)[0]
            new_event_card.free_text_tags.send_keys(tag)
            new_event_card.add_free_text_tags.click()
        new_event_card.are_free_text_tags_added.should.be.true

        new_event_card.publish.click()

        # Creating a new page
        create_new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        create_new_page.title.send_keys(PAGE_TITLE)
        create_new_page.page_builder_section.click()
        click(create_new_page.publish)

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Select single card module
        new_page_builder_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        new_page_builder_page.wpheader.page_builder.click()
        new_page_builder_page.add_single_card(CONTENT_TYPE, EVENT_CARD_INFO['title'])
        wait.until(lambda s: new_page_builder_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        new_page_builder_page.page_builder.done_and_publish()
        new_page_builder_page.log_out_from_header()

        # PART 4 - Patron view
        self.driver.delete_all_cookies()
        page_patron = UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(EVENT_CARD_INFO['title'])
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.true

        self.driver.close()

        # Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

        # PART 5 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_and_delete(PAGE_TITLE)
        all_pages_page.rows.should.be.empty

        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_and_delete(EVENT_CARD_INFO['title'])
        all_cards_page.rows.should.be.empty

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_and_delete(IMAGE_TITLE)
        media_library_page.rows.should.be.empty

        delete_downloaded_image(IMAGE_PATH)
