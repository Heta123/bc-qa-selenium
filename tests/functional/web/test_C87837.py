import pytest
import allure
import sure
import configuration.system
import configuration.user
from mimesis import Text
from selenium.webdriver.support.ui import WebDriverWait
from utils.image_download_helper import *
from utils.selenium_helpers import click
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_new_content.create_new_news_post import CreateNewNewsPostPage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_news import AllNewsPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage
from selenium.common.exceptions import NoSuchElementException


PAGE = 'bibliocommons-settings'
NEWS_INFO = {
    'title': '-'.join(Text('en').words(quantity=3)),
    'subtitle': ' '.join(Text('en').words(quantity=3)),
    'description': Text('en').text(quantity=5)
}
IMAGE_TITLE = '-'.join(Text('en').words(quantity=2))
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "News"


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87837: News Card - Create")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87837", "TestRail")
class TestC87837:
    def test_C87837(self):

        # Log in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web,
                                                                     page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        default_settings_page = DefaultSettingsPageBuilderPage(self.driver, configuration.system.base_url_web,
                                                               post_type='fl-builder-template',
                                                               page='default-page-builder-settings').open()
        default_settings_page.check_all_display_taxonomy_links_checkboxes()
        default_settings_page.save_changes_button_click()

        self.driver.delete_all_cookies()

        # PART 1 - Create a new News Post
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        settings_general_tab = SettingsGeneralTabPage(self.driver, configuration.system.base_url_web, page=PAGE,
                                                      tab='generic').open()
        click(settings_general_tab.disable_structured_tags)
        settings_general_tab.save_changes.click()

        download_image("https://bit.ly/2IOmAJs", IMAGE_PATH)

        new_news_post = CreateNewNewsPostPage(self.driver, configuration.system.base_url_web, post_type='bccms_news').open()
        new_news_post.page_heading.text.should.match("Create New News Post")
        new_news_post.title.send_keys(NEWS_INFO['title'])
        new_news_post.subtitle.send_keys(NEWS_INFO['subtitle'])
        new_news_post.add_media.click()
        new_news_post.insert_media.upload_image.send_keys(IMAGE_PATH)
        new_news_post.insert_media.add_image_to_widget_button.click()
        self.driver.switch_to.frame(new_news_post.visual_area.iframe)
        new_news_post.visual_area.body.send_keys(NEWS_INFO['description'])
        self.driver.switch_to.default_content()

        new_news_post.is_free_text_tags_field_displayed.should.be.true
        num_of_taxonomies = 3
        for i in range(num_of_taxonomies):
            tag = Text('en').words(quantity=1)[0]
            new_news_post.free_text_tags.send_keys(tag)
            new_news_post.add_free_text_tags.click()
        new_news_post.are_free_text_tags_added.should.be.true

        click(new_news_post.autofill_text_fields)
        click(new_news_post.card_image)
        new_news_post.select_widget_image.select_image_from_list(0).click()
        click(new_news_post.select_widget_image.add_image_to_widget_button)
        new_news_post.select_default_image_crop_views()
        new_news_post.scroll_to_top()
        click(new_news_post.publish)

        # Verify news is added to All News page
        all_news_page = AllNewsPage(self.driver, configuration.system.base_url_web, post_type='bccms_news').open()
        all_news_page.search_content_by_button(NEWS_INFO['title'])
        all_news_page.rows[0].title.text.should.match(NEWS_INFO['title'])
        len(all_news_page.rows[0].tags).should.equal(num_of_taxonomies)

        # Verify news is added to All Cards page
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_content_by_button(NEWS_INFO['title'])
        all_cards_page.rows[0].title.text.should.match(NEWS_INFO['title'])
        all_cards_page.rows[0].card_type.text.should.match(CONTENT_TYPE)
        taxonomies = all_cards_page.rows[0].taxonomies
        len(taxonomies).should.equal(num_of_taxonomies)

        # Verify image is added to Media Library page with associated content
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search(IMAGE_TITLE)
        media_library_page.rows[0].title.text.should.match(IMAGE_TITLE)
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(NEWS_INFO['title'])

        # PART 2 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)
        page_staff = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()

        # Select single card module
        page_staff.add_single_card(CONTENT_TYPE, NEWS_INFO['title'])
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda s: page_staff.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        page_staff.page_builder.done_and_publish()
        page_staff.log_out_from_header()

        # PART 3 - Patron view
        self.driver.delete_all_cookies()
        page_patron = UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        len(page_patron.user_facing_modules.single_cards[0].cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.true
        page_patron.user_facing_modules.single_cards[0].cards[0].card_content_type.text.should.match(CONTENT_TYPE.upper())
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(NEWS_INFO['title'])

        # PART 4 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_and_delete(NEWS_INFO['title'])
        all_cards_page.rows.should.be.empty

        all_news_page.open()
        all_news_page.search_content_by_button(NEWS_INFO['title'])
        all_news_page.rows.should.be.empty

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_and_delete(IMAGE_TITLE)
        media_library_page.rows.should.be.empty

        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_and_delete(PAGE_TITLE)
        all_pages_page.rows.should.be.empty

        delete_downloaded_image(IMAGE_PATH)
