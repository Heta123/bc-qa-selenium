import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text
from selenium.webdriver.common.keys import Keys
from utils.selenium_helpers import click
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_catalog_comment_card import CreateNewCatalogCommentCard
from pages.web.staff_base import StaffBasePage
from pages.web.page_builder import PageBuilderPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage
from pages.core.bib_comment import CommentPage


PAGE = 'bibliocommons-settings'
CATALOG_COMMENT_INFO = {
    'item_id': "1900633126",
    'comment_id': "131701788"
}
CORE_BASE_URL = "https://chipublib.stage.bibliocommons.com"
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Catalog Comment"


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87841: Catalog Comment Card (More than 140 chars) - Create")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87841", "TestRail")
class TestC87841:
    def test_C87841(self):

        # Log in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web,
                                                                     page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        default_settings_page = DefaultSettingsPageBuilderPage(self.driver, configuration.system.base_url_web,
                                                               post_type='fl-builder-template',
                                                               page='default-page-builder-settings').open()
        default_settings_page.check_all_display_taxonomy_links_checkboxes()
        default_settings_page.save_changes_button_click()

        self.driver.delete_all_cookies()

        # Log in as Lib Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        settings_general_tab = SettingsGeneralTabPage(self.driver, configuration.system.base_url_web, page=PAGE,
                                                      tab='generic').open()
        click(settings_general_tab.disable_structured_tags)
        settings_general_tab.save_changes.click()

        # Fetch a catalog comment from Core
        comment_page = CommentPage(self.driver, CORE_BASE_URL, item_id=CATALOG_COMMENT_INFO['item_id'], comment_id=CATALOG_COMMENT_INFO['comment_id']).open()
        permalink_url = self.driver.current_url
        bib_title = comment_page.bib_title.text
        bib_author = comment_page.bib_author.text.split(", ")
        bib_author.reverse()
        bib_author = " ".join(bib_author)
        bib_image_link = comment_page.bib_jacket_cover.get_attribute("src")
        bib_comment_author = comment_page.user_id.text
        bib_comment_content = comment_page.comment.text

        # Create a new Catalog Comment card
        new_catalog_comment_card = CreateNewCatalogCommentCard(self.driver, configuration.system.base_url_web,
                                                               post_type='bw_catalog_comment').open()
        new_catalog_comment_card.catalog_comment_url.send_keys(permalink_url)
        new_catalog_comment_card.grab_comment_info.click()
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_catalog_comment_card.is_validator_message_displayed)
        new_catalog_comment_card.is_validator_success_message_displayed.should.be.true
        new_catalog_comment_card.validator_success_message.text.should.match("Catalog Comment information successfully grabbed!")

        new_catalog_comment_card.is_free_text_tags_field_displayed.should.be.true
        num_of_taxonomies = 3
        for i in range(num_of_taxonomies):
            tag = Text('en').words(quantity=1)[0]
            new_catalog_comment_card.free_text_tags.send_keys(tag)
            new_catalog_comment_card.add_free_text_tags.click()
        new_catalog_comment_card.are_free_text_tags_added.should.be.true

        new_catalog_comment_card.scroll_to_top()
        new_catalog_comment_card.publish.click()

        # PART 2 - Verify if it's actually published
        # Verify card is added to All Cards page
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_content_by_button(bib_title)
        all_cards_page.rows[0].title.text.should.match(bib_title)
        all_cards_page.rows[0].card_type.text.should.match(CONTENT_TYPE)
        taxonomies = all_cards_page.rows[0].taxonomies
        len(taxonomies).should.equal(num_of_taxonomies)

        # PART 3 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        page_staff = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()

        # Select single card module
        page_staff.add_single_card(CONTENT_TYPE, bib_title)
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda s: page_staff.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        page_staff.page_builder.done_and_publish()
        page_staff.log_out_from_header()

        # PART 4 - Patron view
        self.driver.delete_all_cookies()
        page_patron = StaffBasePage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        len(page_patron.user_facing_modules.single_cards[0].cards[0].card_tags).should.equal(num_of_taxonomies)

        page_patron.user_facing_modules.single_cards[0].cards[0].card_image.get_attribute("src").should.equal(bib_image_link)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.equal(bib_title)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_bib_author.text.should.equal(bib_author)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_comment_author.text.should.equal(bib_comment_author)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_comment.text[:140].should.equal(bib_comment_content[:140])
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_read_more_displayed.should.be.true

        # PART 5 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_and_delete(bib_title)
        all_cards_page.rows.should.be.empty

        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_and_delete(PAGE_TITLE)
        all_pages_page.rows.should.be.empty
