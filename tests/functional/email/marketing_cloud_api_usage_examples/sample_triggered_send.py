import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })

    # Get all TriggeredSendDefinitions
    print('>>> Get all TriggeredSendDefinitions')
    get_ts = FuelSDK.ET_TriggeredSend()
    get_ts.auth_stub = stubObj
    get_ts.props = ["CustomerKey", "Name", "TriggeredSendStatus"]
    get_response = get_ts.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Count: ' + str(len(get_response.results)))
    # print 'Results: ' + str(get_response.results)
            
    # Specify the name of a TriggeredSend that was setuprint for testing 
    # Do not use a production Triggered Send Definition

    name_of_test_ts = "TEXTEXT"
    
    # Pause a TriggeredSend
    
    print('>>> Pause a TriggeredSend')
    patch_trig = FuelSDK.ET_TriggeredSend()
    patch_trig.auth_stub = stubObj
    patch_trig.props = {"CustomerKey": name_of_test_ts, "TriggeredSendStatus": "Inactive"}
    patch_response = patch_trig.patch()
    print('Patch Status: ' + str(patch_response.status))
    print('Code: ' + str(patch_response.code))
    print('Message: ' + str(patch_response.message))
    print('Result Count: ' + str(len(patch_response.results)))
    print('Results: ' + str(patch_response.results))

    # Retrieve Single TriggeredSend
    print('>>> Retrieve Single TriggeredSend')
    get_ts = FuelSDK.ET_TriggeredSend()
    get_ts.auth_stub = stubObj
    get_ts.props = ["CustomerKey", "Name", "TriggeredSendStatus"]
    get_ts.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': name_of_test_ts}
    get_response = get_ts.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Count: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))
    
    # Start a TriggeredSend by setting to Active
    print('>>> Start a TriggeredSend by setting to Active')
    patch_trig = FuelSDK.ET_TriggeredSend()
    patch_trig.auth_stub = stubObj
    patch_trig.props = {"CustomerKey": name_of_test_ts, "TriggeredSendStatus": "Active"}
    patch_response = patch_trig.patch()
    print('Patch Status: ' + str(patch_response.status))
    print('Code: ' + str(patch_response.code))
    print('Message: ' + str(patch_response.message))
    print('Result Count: ' + str(len(patch_response.results)))
    print('Results: ' + str(patch_response.results))
    
    # Retrieve Single TriggeredSend After setting back to active
    print('>>> Retrieve Single TriggeredSend After setting back to active')
    get_ts = FuelSDK.ET_TriggeredSend()
    get_ts.auth_stub = stubObj
    get_ts.props = ["CustomerKey", "Name", "TriggeredSendStatus"]
    get_ts.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': name_of_test_ts}
    get_response = get_ts.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Count: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))

    # Send an email with TriggeredSend
    print('>>> Send an email with TriggeredSend')
    send_trig = FuelSDK.ET_TriggeredSend()
    send_trig.auth_stub = stubObj
    send_trig.props = {"CustomerKey": name_of_test_ts}
    send_trig.subscribers = [{"EmailAddress": "testing@bh.exacttarget.com", "SubscriberKey": "testing@bh.exacttarget.com"}]
    send_response = send_trig.send()
    print('Send Status: ' + str(send_response.status))
    print('Code: ' + str(send_response.code))
    print('Message: ' + str(send_response.message))
    print('Result Count: ' + str(len(send_response.results)))
    print('Results: ' + str(send_response.results))
    
    # Generate a unique identifier for the TriggeredSend customer key since they cannot be re-used even after deleted
    ts_name_for_create_then_delete = str(uuid.uuid4())
    
    # Create a TriggeredSend Definition 
    print('>>> Create a TriggeredSend Definition')
    post_trig = FuelSDK.ET_TriggeredSend()
    post_trig.auth_stub = stubObj
    post_trig.props = {'CustomerKey': ts_name_for_create_then_delete, 'Name': ts_name_for_create_then_delete, 'Email': {"ID": "3113962"}, "SendClassification": {"CustomerKey": "2240"}}
    post_response = post_trig.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Result Count: ' + str(len(post_response.results)))
    print('Results: ' + str(post_response.results))
    
    # Delete a TriggeredSend Definition 
    print('>>> Delete a TriggeredSend Definition ')
    delete_trig = FuelSDK.ET_TriggeredSend()
    delete_trig.auth_stub = stubObj
    delete_trig.props = {'CustomerKey': ts_name_for_create_then_delete}
    delete_response = delete_trig.delete()
    print('Delete Status: ' + str(delete_response.status))
    print('Code: ' + str(delete_response.code))
    print('Message: ' + str(delete_response.message))
    print('Result Count: ' + str(len(delete_response.results)))
    print('Results: ' + str(delete_response.results))

except Exception as e:
    print('Caught exception: ' + str(e))
