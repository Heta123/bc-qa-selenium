import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
  
    # Retrieve All ContentArea with GetMoreResults
    print('>>> Retrieve All ContentArea with GetMoreResults')
    get_content = FuelSDK.ET_ContentArea()
    get_content.auth_stub = stubObj
    get_content.props = ["RowObjectID", "ObjectID", "ID", "CustomerKey", "Client.ID", "ModifiedDate", "CreatedDate", "CategoryID", "Name", "Layout",
                        "IsDynamicContent", "Content", "IsSurvey", "IsBlank", "Key"]
    get_response = get_content.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    # print 'Results: ' + str(get_response.results)

    while get_response.more_results:
        print('>>> Continue Retrieve All ContentArea with GetMoreResults')
        get_response = get_content.getMoreResults()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('RequestID: ' + str(get_response.request_id))
        print('Results Length: ' + str(len(get_response.results)))

    NameOfTestContentArea = "PythonSDKContentArea"

    # Create ContentArea 
    print('>>> Create ContentArea')
    post_content = FuelSDK.ET_ContentArea()
    post_content.auth_stub = stubObj
    post_content.props = {"CustomerKey": NameOfTestContentArea, "Name": NameOfTestContentArea, "Content": "<b>Some HTML Content Goes here</b>"}
    post_response = post_content.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Result Count: ' + str(len(post_response.results)))
    print('Results: ' + str(post_response.results))
  
    # Retrieve newly created ContentArea
    print('>>> Retrieve newly created ContentArea')
    get_content = FuelSDK.ET_ContentArea()
    get_content.auth_stub = stubObj
    get_content.props = ["RowObjectID", "ObjectID", "ID", "CustomerKey", "Client.ID", "ModifiedDate", "CreatedDate", "CategoryID", "Name", "Layout",
                        "IsDynamicContent", "Content", "IsSurvey", "IsBlank", "Key"]
    get_content.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': NameOfTestContentArea}
    get_response = get_content.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))
    
    # Update ContentArea 
    print('>>> Update ContentArea')
    patch_content = FuelSDK.ET_ContentArea()
    patch_content.auth_stub = stubObj
    patch_content.props = {"CustomerKey": NameOfTestContentArea, "Name": NameOfTestContentArea, "Content": "<b>Some HTML Content Goes here. NOW WITH NEW CONTENT</b>"}
    patch_response = patch_content.patch()
    print('Patch Status: ' + str(patch_response.status))
    print('Code: ' + str(patch_response.code))
    print('Message: ' + str(patch_response.message))
    print('Result Count: ' + str(len(patch_response.results)))
    print('Results: ' + str(patch_response.results))
    
    # Retrieve updated ContentArea
    print('>>> Retrieve updated ContentArea')
    get_content = FuelSDK.ET_ContentArea()
    get_content.auth_stub = stubObj
    get_content.props = ["RowObjectID", "ObjectID", "ID", "CustomerKey", "Client.ID", "ModifiedDate", "CreatedDate", "CategoryID", "Name", "Layout", "IsDynamicContent",
                        "Content", "IsSurvey", "IsBlank", "Key"]
    get_content.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': NameOfTestContentArea}
    get_response = get_content.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))
        
    # Delete ContentArea 
    print('>>> Delete ContentArea')
    delete_content = FuelSDK.ET_ContentArea()
    delete_content.auth_stub = stubObj
    delete_content.props = {"CustomerKey": NameOfTestContentArea, "Name": NameOfTestContentArea, "Content": "<b>Some HTML Content Goes here. NOW WITH NEW CONTENT</b>"}
    delete_response = delete_content.delete()
    print('Delete Status: ' + str(delete_response.status))
    print('Code: ' + str(delete_response.code))
    print('Message: ' + str(delete_response.message))
    print('Result Count: ' + str(len(delete_response.results)))
    print('Results: ' + str(delete_response.results))
        
    # Retrieve ContentArea to confirm deletion
    print('>>> Retrieve ContentArea to confirm deletion')
    get_content = FuelSDK.ET_ContentArea()
    get_content.auth_stub = stubObj
    get_content.props = ["RowObjectID", "ObjectID", "ID", "CustomerKey", "Client.ID", "ModifiedDate", "CreatedDate", "CategoryID", "Name", "Layout", "IsDynamicContent",
                        "Content", "IsSurvey", "IsBlank", "Key"]
    get_content.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': NameOfTestContentArea}
    get_response = get_content.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))

except Exception as e:
    print('Caught exception: ' + str(e))
