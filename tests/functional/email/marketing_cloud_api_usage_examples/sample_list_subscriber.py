import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
    
    # NOTE: These examples only work in accounts where the SubscriberKey functionality is not enabled
    #       SubscriberKey will need to be included in the props if that feature is enabled
    
    new_list_name = "PythonSDKListSubscriber"
    subscriber_test_email = "PythonSDKListSubscriber@bh.exacttarget.com"
    
    # Create List 
    print('>>> Create List')
    post_list = FuelSDK.ET_List()
    post_list.auth_stub = stubObj
    post_list.props = {"ListName": new_list_name, "Description": "This list was created with the PythonSDK", "Type": "Private"}
    post_response = post_list.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Result Count: ' + str(len(post_response.results)))
    print('Results: ' + str(post_response.results))

    # Make sure the list created correctly before 
    if post_response.status:
        
        new_list_id = post_response.results[0]['NewID']
    
        # Create Subscriber On List 
        print('>>> Create Subscriber On List')
        post_sub = FuelSDK.ET_Subscriber()
        post_sub.auth_stub = stubObj
        post_sub.props = {"EmailAddress": subscriber_test_email, "Lists": [{"ID": new_list_id}]}
        post_response = post_sub.post()
        print('Post Status: ' + str(post_response.status))
        print('Code: ' + str(post_response.code))
        print('Message: ' + str(post_response.message))
        print('Result Count: ' + str(len(post_response.results)))
        print('Results: ' + str(post_response.results))
       
        if post_response.status is False:
            # If the subscriber already exists in the account then we need to do an update.
            # Update Subscriber On List 
            if post_response.results[0]['ErrorCode'] == 12014:
                # Update Subscriber to add to List
                print('>>> Update Subscriber to add to List')
                patch_sub = FuelSDK.ET_Subscriber()
                patch_sub.auth_stub = stubObj
                patch_sub.props = {"EmailAddress": subscriber_test_email, "Lists": [{"ID": new_list_id}]}
                patch_response = patch_sub.patch()
                print('Patch Status: ' + str(patch_response.status))
                print('Code: ' + str(patch_response.code))
                print('Message: ' + str(patch_response.message))
                print('Result Count: ' + str(len(patch_response.results)))
                print('Results: ' + str(patch_response.results))
        
        # Retrieve all Subscribers on the List
        print('>>> Retrieve all Subscribers on the List')
        get_list_subs = FuelSDK.ET_List_Subscriber()
        get_list_subs.auth_stub = stubObj
        get_list_subs.props = ["ObjectID", "SubscriberKey", "CreatedDate", "Client.ID", "Client.PartnerClientKey", "ListID", "Status"]
        get_list_subs.search_filter = {'Property': 'ListID', 'SimpleOperator': 'equals', 'Value': new_list_id}
        get_response = get_list_subs.get()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('Results Length: ' + str(len(get_response.results)))
        print('Results: ' + str(get_response.results))
        
        # Delete List
        print('>>> Delete List')
        delete_sub = FuelSDK.ET_List()
        delete_sub.auth_stub = stubObj
        delete_sub.props = {"ID": new_list_id}
        delete_response = delete_sub.delete()
        print('Delete Status: ' + str(delete_response.status))
        print('Code: ' + str(delete_response.code))
        print('Message: ' + str(delete_response.message))
        print('Results Length: ' + str(len(delete_response.results)))
        print('Results: ' + str(delete_response.results))

except Exception as e:
    print('Caught exception: ' + str(e))
