import pytest
import allure
import sys
sys.path.append('tests')
# noinspection PyUnresolvedReferences
import sure
from pages.events.events_locations import EventLocationPage
from utils.selenium_helpers import click


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("Filter locations by facilities")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56141", "TestRail")
class TestC56141:
    def test_c56141(self):

        self.base_url = "http://chipublib.local.bibliocommons.com/locations/"
        location_page = EventLocationPage(self.driver, self.base_url).open()
        zip_code = "60605"
        location_page.wait.until(lambda s: location_page.is_find_by_address_or_zip_displayed)
        location_page.find_by_address_or_zip.send_keys(zip_code)
        location_page.search_button[4].click()

        location_page = EventLocationPage(self.driver)
        location_page.wait.until(lambda s: location_page.is_search_results_returned_count_displayed)
        store_results_count = location_page.search_results_returned_count.text
        location_page.wait.until(lambda s: location_page.library_in_list_name[0].is_displayed())

        location_page.wait.until(lambda s: location_page.is_show_locations_with_filter_button_displayed)
        click(location_page.locations_with_filter_button)
        location_page.wait.until(lambda s: location_page.show_locations_list_with_checkbox[1].is_displayed())
        location_page.show_locations_list_with_checkbox[3].click()
        location_page.wait.until(lambda s: location_page.is_show_locations_list_selected_clear_filters_link_displayed)

        location_page.wait.until(lambda s: location_page.is_show_locations_with_filter_button_displayed)
        click(location_page.locations_with_filter_button)
        location_page.wait.until(lambda s: location_page.show_locations_list_with_checkbox[2].is_displayed())
        location_page.show_locations_list_with_checkbox[4].click()
        location_page.wait.until(lambda s: location_page.is_show_locations_list_selected_clear_filters_link_displayed)

        location_page.wait.until(lambda s: location_page.is_show_locations_with_filter_button_displayed)
        click(location_page.locations_with_filter_button)
        location_page.wait.until(lambda s: location_page.show_locations_list_with_checkbox[11].is_displayed())
        location_page.show_locations_list_with_checkbox[6].click()
        location_page.wait.until(lambda s: location_page.is_events_location_first_library_in_list_displayed)

        store_results_count_after_filters = location_page.search_results_returned_count.text
        first_result = "".join(i for i in store_results_count if i.isdigit())
        second_result = "".join(i for i in store_results_count_after_filters if i.isdigit())
        second_result.should.be.lower_than(first_result)
