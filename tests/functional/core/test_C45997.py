import pytest
import allure
import sure
import sys
sys.path.append('tests')
from datetime import date, timedelta
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.v2.holds import HoldsPage
from utils.bc_test_connector import TestConnector

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C45997: Select a hold and suspend")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C45997", "TestRail")
class TestC45997:
    def test_C45997(self):
        self.user = TestConnector.User().create(self.driver)

        _next_date = date.today() + timedelta(days=2)
        self._next_date_string = _next_date.strftime("%b. %d, %Y")

        home_page = HomePage(self.driver, configuration.system.base_url).open()
        home_page.header.log_in(self.user.barcode, self.user.pin)
        home_page.header.search_for("Harry Potter")
        search_page = SearchResultsPage(self.driver)
        search_page.wait_for_page_to_load()
        search_page.search_result_items[0].place_hold.click()
        search_page.wait.until(lambda s: search_page.search_result_items[0].is_confirm_hold_displayed)
        search_page.search_result_items[0].confirm_hold.click()
        search_page.wait.until(lambda s: search_page.search_result_items[0].is_cancel_hold_displayed)
        search_page.search_result_items[0].is_cancel_hold_displayed.should.be.true

        holds_page = HoldsPage(self.driver, configuration.system.base_url).open()
        holds_page.wait.until(lambda s: holds_page.holds_list_is_displayed)
        holds_page.items[0].pause_hold.click()
        holds_page.wait.until(lambda s: holds_page.items[0].is_pick_an_end_date_displayed)
        holds_page.items[0].pick_an_end_date.click()
        holds_page.wait.until(lambda s: holds_page.overlay.suspend_end_date.displayed)
        holds_page.overlay.suspend_end_date.suspend_end_date.click()
        holds_page.wait.until(lambda s: holds_page.items[0].is_confirm_pause_hold_displayed)

        try:
            holds_page.items[0].confirm_pause_hold.click()
        except WebDriverException:
            holds_page.items[0].confirm_pause_hold.click()

        holds_page.wait.until(lambda s: holds_page.is_success_notification_displayed)
        holds_page.items[0].paused_until.text.should.contain(self._next_date_string)
        holds_page.is_success_notification_displayed.should.be.true
        holds_page.items[0].item_status.text.should.contain("Paused")

        holds_page = HoldsPage(self.driver)
        holds_page.items[0].cancel.click()
        holds_page.items[0].confirm.click()
