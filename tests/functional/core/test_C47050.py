import pytest
import allure
import re
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.fines import FinesPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47050: Check Fines page")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47050", "TestRail")
class TestC47050:
    def test_C47050(self):
        self.base_url = "https://epl.demo.bibliocommons.com"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.log_in("21221011111111", "1234")
        home_page.header.login_state_user_logged_in.click()
        home_page.header.fees.click()
        fines_page = FinesPage(self.driver)
        # Should find a match for 2 decimal places & 'Due' text:
        fines_page.fines_payable.text.should.match(r"\.\d{2} Due")
