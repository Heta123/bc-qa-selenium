import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

# from pages.core. ... import ...

@pytest.mark.skip(reason = 'TBD')
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46957: Send Explore -> New Titles")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46957", "TestRail")
class TestC46957:
    def test_C46957(self):
        ('TBD')
