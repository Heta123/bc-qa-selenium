import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

# from pages.core. ... import ...

@pytest.mark.skip(reason = 'TBD')
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C45999: Shelves page count match items")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C45999", "TestRail")
class TestC45999:
    def test_C45999(self):
        ('TBD')
