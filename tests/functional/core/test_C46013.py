import pytest
import allure
import sure
import sys
sys.path.append('tests')
import datetime
import configuration.system

from pages.core.home import HomePage
from pages.core.user_dashboard import UserDashboardPage
from pages.core.suggested_purchases import SuggestedPurchasesPage
from selenium.common.exceptions import TimeoutException, ElementClickInterceptedException
from pages.core.admin_suggested_purchases import AdminSuggestedPurchasesPage
from selenium.webdriver.support.select import Select
from pages.core.components.overlay import Overlay
from utils.bc_test_connector import TestConnector

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46013: Login and Suggest an item for Purchase (EPL, YPRL, Ottawa, KCLS)")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46013", "TestRail")
class TestC46013:
    def test_C46013(self):
        ### This test can be used as a template for Suggest For Purchase behaviors that are not currently tested, eg
        # -Suggesting an item that has already been suggested
        # -Attempting to submit more suggestions than a library allows
        # -Future upcoming changes to S4P

        # Log in
        home_page = HomePage(self.driver, "https://coaldale.demo.bibliocommons.com").open()
        home_page.header.log_in("21817002369272", "1992")

        # Go to setting page and click on suggested purchases

        home_page.header.login_state_user_logged_in.click()
        home_page.header.my_library_dashboard.click()
        dashboard_page = UserDashboardPage(self.driver)

        # This number needs to be between 1 and the max number of allowed suggestions at the test library
        number_of_suggestions = 2

        if number_of_suggestions == 1:

            suggestion_count_message = "Showing 1 suggestion."
        else:
            suggestion_count_message = "Showing " + str(number_of_suggestions) + " suggestions."

        #
        # {Repeatedly} submit suggestions

        for submission in range(number_of_suggestions):
            # Enter overlay
            dashboard_page.wait.until(lambda s: dashboard_page.is_submit_suggestion_displayed)
            dashboard_page.submit_suggestion.click()

            dashboard_page.wait.until(lambda s: dashboard_page.overlay.submit_suggestion.is_loaded)

            # Using `Bibliocommons Test Entry - YYYY-MM-DD` as the bib title for easy identification later
            # Year as 2018 and author as Bibliocommons
            dashboard_page.overlay.submit_suggestion.title.send_keys("Bibliocommons Test Entry - " +
                                            datetime.datetime.today().strftime('%Y-%m-%d') + " " + str(submission + 1))
            dashboard_page.overlay.submit_suggestion.author.send_keys("Bibliocommons")
            dashboard_page.overlay.submit_suggestion.publication_year.send_keys("2018")

            # This block is used to make sure selenium hasn't clicked the "Next" or "Submit" buttons too quickly.
            # Compare the URL before and after clicking to see if the button worked. Tries again one time.
            prior_url = self.driver.current_url
            dashboard_page.overlay.submit_suggestion.next_step.click()
            try:
                dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url, 0.8)
            except TimeoutException:
                dashboard_page.overlay.submit_suggestion.next_step.click()
                dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url)
            #

            # On the next page, select values from the required dropdown menus
            dashboard_page.wait.until(lambda s: dashboard_page.overlay.submit_suggestion.is_dropdown_displayed)

            dashboard_page.overlay.submit_suggestion.select_from_dropdown(list='format', value='DVD')
            dashboard_page.overlay.submit_suggestion.select_from_dropdown(list='audience', value='Adult')
            dashboard_page.overlay.submit_suggestion.select_from_dropdown(list='content', value='Fiction')

            # Click next and click submit. Ensure Both clicks worked properly
            #
            prior_url = self.driver.current_url
            dashboard_page.overlay.submit_suggestion.next_step.click()
            try:
                dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url, 0.8)
            except TimeoutException:
                dashboard_page.overlay.submit_suggestion.next_step.click()
                dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url)
            #

            dashboard_page.wait.until(lambda s: dashboard_page.overlay.submit_suggestion.is_submit_button_displayed)

            #
            prior_url = self.driver.current_url
            dashboard_page.overlay.submit_suggestion.submit_button.click()
            try:
                dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url, 0.8)
            except TimeoutException:
                dashboard_page.overlay.submit_suggestion.submit_button.click()
                dashboard_page.wait.until(lambda s: self.driver.current_url != prior_url)
            #
            #

        # Navigate to the Suggested Purchases page from the user dashboard
        # Sometimes Selenium attempts to click this link while the overlay is still closing
        dashboard_page.wait.until_not(lambda s: dashboard_page.overlay.submit_suggestion.is_submit_button_displayed)
        dashboard_page.wait.until(lambda s: dashboard_page.is_suggest_for_purchase_link_displayed)
        dashboard_page.suggest_for_purchase_link.click()

        suggested_purchases_page = SuggestedPurchasesPage(self.driver)

        # The number in the suggestion counter must match the number of suggestions we have made
        # "Showing x suggestions."
        suggested_purchases_page.suggestion_counter.text.should.equal(suggestion_count_message)

        # Cancel all of the suggestions that have been made
        for i in range(number_of_suggestions):
            suggested_purchases_page.wait.until(lambda s: suggested_purchases_page.is_cancel_suggestion_displayed)
            suggested_purchases_page.cancel_suggestion.click()

            suggested_purchases_page.wait.until(lambda s: suggested_purchases_page.is_cancel_confirmation_displayed)
            suggested_purchases_page.cancel_confirmation.click()

        # Assert that zero suggestions are left on the submitted suggestions list

        suggested_purchases_page.wait.until(lambda s: suggested_purchases_page.suggestion_counter.text == "Showing 0 suggestions.")
        suggested_purchases_page.suggestion_counter.text.should.equal("Showing 0 suggestions.")
