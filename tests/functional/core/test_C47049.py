import pytest
import allure
import random
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.staff_picks import StaffPicksPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47049: Explore Staff Picks")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47049", "TestRail")
class TestC47049:
    def test_C47049(self):
        self.base_url = "https://sfpl.demo.bibliocommons.com"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.explore.click()
        home_page.header.staff_picks.click()
        staff_picks_page = StaffPicksPage(self.driver)

        list = random.choice(staff_picks_page.panels)
        print(list.title.text)
        link = random.choice(list.links)
        print(link.text)
        link.click()
