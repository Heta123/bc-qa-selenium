import pytest
import allure
import sure
import time
import datetime
import sys
sys.path.append('tests')
import configuration.system

from pages.core.bib import BibPage
from pages.core.list_editor import ListEditorPage
from pages.core.user_lists import UserListsPage
from pages.core.user_dashboard import UserDashboardPage
from pages.core.lists import ListsPage
from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from utils.bc_test_connector import TestConnector

@pytest.fixture(scope='class')
def setup_shared_user(request, selenium_setup_and_teardown):
    driver = request.cls.driver
    request.cls.user = TestConnector.User().create(driver)
    request.cls.item_id = "2560434030"
    request.cls.item_id_2 = "719979001"

@pytest.mark.usefixtures('setup_shared_user')
class TestLists:
    @allure.title("C46003: Create a new list")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46003", "TestRail")
    def test_C46003(self):
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.wait.until(lambda s: bib_page.ugc_metadata.loaded)
        bib_page.header.log_in(self.user.barcode, self.user.pin)
        bib_page.ugc_metadata.add_to_list.click()
        bib_page.ugc_metadata.create_a_new_list.click()
        list_editor_page = ListEditorPage(self.driver)
        list_editor_page.wait.until(lambda s: list_editor_page.loaded)
        list_editor_page.guides_and_recos_list.click()
        user_lists_page = UserListsPage(self.driver)
        user_lists_page.edit_list_name.click()
        # Use current DateTime to create a unique identifier for the List title:
        user_lists_page.title.send_keys("Web Automation [{}]".format(datetime.datetime.now().strftime("%H%M%S%f")))
        user_lists_page.edit_list_description.click()
        user_lists_page.description.send_keys("Added by Python web automation...")
        items = [
        "https://bellingham.bibliocommons.com/list/share/110058314_bpladultlibrarians/1135529207_latin_american_fiction",
        "https://seattle.bibliocommons.com/list/share/86922331_seattle_quick_picks/1143326987_metoo_sexual_harassment,_sexual_violence,_and_consent",
        "https://seattle.bibliocommons.com/list/share/86922331_seattle_quick_picks/1145250767_2017_nebula_award_finalists",
        "https://seattle.bibliocommons.com/list/share/86922331_seattle_quick_picks/1157450727_artswests_an_octoroon_beyond_the_theatre"
        ]
        for item in items:
            user_lists_page.add_to_list.click()
            try:
                user_lists_page.add_item_to_list_web_url_tab.click()
            except:
                user_lists_page.add_to_list.click()
                user_lists_page.add_item_to_list_web_url_tab.click()
            user_lists_page.add_item_to_list_web_url.send_keys(item)
            user_lists_page.add_item_to_list_web_url_okay.click()
            user_lists_page.wait.until(lambda s: user_lists_page.is_add_item_to_list_web_item_title_displayed)
            user_lists_page.wait.until(lambda s: len(user_lists_page.add_item_to_list_web_item_title.get_attribute("value")) > 0)
            user_lists_page.add_item_to_list_add_web_item.click()
        user_lists_page.finished_editing.click()
        user_lists_page.wait.until(lambda s: user_lists_page.is_overlay_publish_displayed)
        time.sleep(1)
        user_lists_page.overlay_publish.click()
        user_lists_page.wait.until(lambda s: user_lists_page.is_list_published_alert_displayed)
        user_lists_page.header.login_state_user_logged_in.click()
        user_lists_page.header.my_library_dashboard.click()
        user_dashboard_page = UserDashboardPage(self.driver)
        user_dashboard_page.wait.until(lambda s: user_dashboard_page.my_collections.loaded)
        user_dashboard_page.my_collections.lists_count.text.should.equal('1')   # Count is a string

    @allure.title("C46002: Save to a List")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46002", "TestRail")
    def test_C46002(self):
        # Delete all cookies since the browser is not restarted between these tests:
        self.driver.delete_all_cookies()

        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        # # bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id_2).open()
        bib_page.wait.until(lambda s: bib_page.ugc_metadata.loaded)
        bib_page.header.log_in(self.user.barcode, self.user.pin)
        bib_page.ugc_metadata.add_to_list.click()
        bib_page.ugc_metadata.existing_lists.click()
        time.sleep(1)
        bib_page.overlay.create_draft_and_add[0].click()
        bib_page.wait.until(lambda s: (bib_page.overlay.list_actions[1].text == "In Draft"))
        bib_page.overlay.close.click()
        bib_page.header.login_state_user_logged_in.click()
        bib_page.header.my_collections_lists.click()
        lists_page = ListsPage(self.driver)
        lists_page.draft_lists[1].edit.click()
        user_lists_page = UserListsPage(self.driver)
        user_lists_page.finished_editing.click()
        user_lists_page.wait.until(lambda s: user_lists_page.is_overlay_publish_displayed)
        time.sleep(1)
        user_lists_page.overlay_publish.click()
        user_lists_page.wait.until(lambda s: user_lists_page.is_list_published_alert_displayed)
        user_lists_page.list_published_alert.text.should.equal("Your list was successfully published. Thank you for your contribution!")

    @allure.title("C46004: Click Add a Catalog Item and add a second item to the list")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46004", "TestRail")
    def test_C46004(self):
        # Delete all cookies since the browser is not restarted between these tests:
        self.driver.delete_all_cookies()

        home_page = HomePage(self.driver, configuration.system.base_url).open()
        home_page.header.log_in(self.user.barcode, self.user.pin)
        search_results_page = home_page.header.search_for("No Way Back")
        search_results_page = SearchResultsPage(self.driver)
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.search_result_items[0].bib_title.click()
        bib_page = BibPage(self.driver)
        bib_page.wait.until(lambda s: bib_page.ugc_metadata.loaded)
        bib_page.ugc_metadata.add_to_list.click()
        bib_page.ugc_metadata.existing_lists.click()
        bib_page.overlay.add_to_list.create_draft_and_add[0].click()
        bib_page.wait.until(lambda s: (bib_page.overlay.add_to_list.list_actions[1].text == "In Draft"))
        bib_page.overlay.add_to_list.close.click()
        bib_page.header.login_state_user_logged_in.click()
        bib_page.header.my_collections_lists.click()
        lists_page = ListsPage(self.driver)
        lists_page.draft_lists[1].edit.click()
        user_lists_page = UserListsPage(self.driver)
        user_lists_page.add_to_list.click()
        user_lists_page.add_item_to_list_catalogue_item_search.send_keys("Knitting", Keys.RETURN)
        user_lists_page.wait.until(lambda s: (len(user_lists_page.add_item_to_list_catalogue_item_search_result_items) > 0))
        user_lists_page.add_item_to_list_catalogue_item_search_result_add[0].click()
        user_lists_page.finished_editing.click()
        user_lists_page.overlay.ready_to_publish.only_me.click()
        user_lists_page.overlay.ready_to_publish.publish.click()
        user_lists_page.wait.until(lambda s: user_lists_page.is_list_published_alert_displayed)
        user_lists_page.list_published_alert.text.should.equal("Your list has been privately published. Only you will be able to see your list.")

    @allure.title("C46005: Create a Personal Recommendation list")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46005", "TestRail")
    def test_C46005(self):
        # Delete all cookies since the browser is not restarted between these tests:
        self.driver.delete_all_cookies()

        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        # bib_page.wait.until(lambda s: bib_page.ugc_metadata.loaded)
        bib_page.header.log_in(self.user.barcode, self.user.pin)
        bib_page.ugc_metadata.add_to_list.click()
        bib_page.ugc_metadata.create_a_new_list.click()
        list_editor_page = ListEditorPage(self.driver)
        # list_editor_page.wait.until(lambda s: list_editor_page.loaded)
        list_editor_page.guides_and_recos_list.click()
        user_lists_page = UserListsPage(self.driver)
        user_lists_page.list_type_dropdown.click()
        user_lists_page.wait.until(lambda s: user_lists_page.is_list_type_menu_displayed)
        user_lists_page.list_type_personal_recommendation.click()
        user_lists_page.edit_list_name.click()
