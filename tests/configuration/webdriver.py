from selenium import webdriver

HUB = 'http://localhost:4444/wd/hub'

class WebDriver:

    def __init__(self, browser = 'Chrome', execution = 'Remote'):
        if browser == 'Chrome':
            options = webdriver.ChromeOptions()
            # options.add_argument("headless")
            # options.add_argument("window-size=1920,1080") # ("window-size=800,600")

            if execution == 'Local':
                self.instance = webdriver.Chrome(executable_path = "../drivers/chromedriver", chrome_options = options)
            else:
                capabilities = options.to_capabilities()
                self.instance = webdriver.Remote(desired_capabilities = capabilities, command_executor = HUB)

        if browser == 'Firefox':
            options = webdriver.FirefoxOptions()

            if execution == 'Local':
                # self.instance = webdriver.Firefox(executable_path = "../drivers/geckodriver", firefox_options = options)
                self.instance = webdriver.Firefox(executable_path = "/Users/johnkolokotronis/selenium/drivers/geckodriver", firefox_options = options)
            else:
                capabilities = options.to_capabilities()
                self.instance = webdriver.Remote(desired_capabilities = capabilities, command_executor = HUB)

        # self.instance.switch_to_window(self.instance.current_window_handle)
