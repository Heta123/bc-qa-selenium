from selenium.webdriver.common.by import By
from pypom import Region


class ShelfItem(Region):
    _checkbox_locator = (By.CSS_SELECTOR, "[aria-labelledby*='label_batch-actions-list-item-checkbox']")
    _title_locator = (By.CSS_SELECTOR, "[data-key='bib-title']")
    _subtitle_locator = (By.CLASS_NAME, "cp-subtitle")
    _author_locator = (By.CLASS_NAME, "author-link")
    _manage_item_locator = (By.CSS_SELECTOR, "[class*='btn-on-shelf dropdown-toggle']")
    _remove_from_shelves_locator = (By.CSS_SELECTOR, "[class='shelf-delete']")

    @property
    def checkbox(self):
        return self.find_element(*self._checkbox_locator)

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def subtitle(self):
        return self.find_element(*self._subtitle_locator)

    @property
    def author(self):
        return self.find_element(*self._author_locator)

    @property
    def manage_item(self):
        return self.find_element(*self._manage_item_locator)

    @property
    def remove_from_shelves(self):
        return self.find_element(*self._remove_from_shelves_locator)
