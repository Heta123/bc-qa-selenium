from pypom import Region
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import NoSuchElementException

class CardTaxonomies(Region):

    _root_locator = (By.CLASS_NAME, "form-wrap")
    _name_label_locator= (By.CSS_SELECTOR, "[for='tag-name']")
    _name_textbox_locator = (By.ID, "tag-name")
    _slug_label_locator = (By.CSS_SELECTOR, "[for='tag-slug']")
    _slug_textbox_locator = (By.ID, "tag-slug")

    _description_label_locator = (By.CSS_SELECTOR, "[for='tag-description']")
    _description_textarea_locator = (By.ID, "tag-description")

    @property
    def is_name_label_displayed(self):
        return self.find_element(*self._name_label_locator)

    @property
    def input_name(self):
        return self.find_element(*self._name_textbox_locator)

    @property
    def is_slug_label_displayed(self):
        return self.find_element(*self._slug_label_locator)

    @property
    def input_slug(self):
        return self.find_element(*self._slug_textbox_locator)

    @property
    def is_description_label_displayed(self):
        return self.find_element(*self._description_label_locator)

    @property
    def input_description(self):
        return self.find_element(*self._description_textarea_locator)


class TaxonomiesTable(Region):  # Class for taxonomies sorting, bulk-actions, count

    _bulk_actions_top_dropdown_locator = (By.ID, "bulk-action-selector-top")
    _apply_button_top_locator = (By.ID, "doaction")

    _table_name_sort_locator = (By.CSS_SELECTOR, "[href*='orderby=name']")
    _table_description_sort_locator = (By.CSS_SELECTOR, "[href*='orderby=description']")
    _table_slug_sort_locator = (By.CSS_SELECTOR, "[href*='orderby=slug']")
    _table_count_sort_locator = (By.CSS_SELECTOR, "[href*='orderby=count']")

    _count_items_locator = (By.CLASS_NAME, "displaying-num")

    _bulk_actions_bottom_dropdown_locator = (By.ID, "bulk-action-selector-bottom")
    _apply_button_bottom_locator = (By.ID, "doaction2")

    dropdown_options=["Bulk Actions", "Delete"]

    def select_bulk_action_top_dropdown(self,value):
        sel = Select(self.find_element(*self._bulk_actions_top_dropdown_locator))
        if value in self.dropdown_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    @property
    def apply_button_top_click(self):
        return self.find_element(*self._apply_button_top_locator)

    @property
    def table_name_top_sort_click(self):
        elements = self.find_elements(*self._table_name_sort_locator)
        return elements[0]

    @property
    def table_description_top_sort_click(self):
        elements = self.find_elements(*self._table_description_sort_locator)
        return elements[0]

    @property
    def table_slug_top_sort_click(self):
        elements = self.find_elements(*self._table_slug_sort_locator)
        return elements[0]

    @property
    def table_count_top_sort_click(self):
        elements= self.find_elements(*self._table_count_sort_locator)
        return elements[0]

    @property
    def table_name_bottom_sort_click(self):
        elements = self.find_elements(*self._table_name_sort_locator)
        return elements[1]

    @property
    def table_description_bottom_sort_click(self):
        elements = self.find_elements(*self._table_description_sort_locator)
        return elements[1]

    @property
    def table_slug_bottom_sort_click(self):
        elements = self.find_elements(*self._table_slug_sort_locator)
        return elements[1]

    @property
    def table_count_bottom_sort_click(self):
        elements = self.find_elements(*self._table_count_sort_locator)
        return elements[1]

    @property
    def count_items_top(self):
        element = self.find_elements(*self._count_items_locator)
        return element[0]

    def select_bulk_action_bottom_dropdown(self, value):
        sel = Select(self.find_element(*self._bulk_actions_bottom_dropdown_locator))
        if value in self.dropdown_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    @property
    def apply_button_bottom_click(self):
        return self.find_element(*self._apply_button_bottom_locator)

    @property
    def count_items_bottom(self):
        element = self.find_elements(*self._count_items_locator)
        return element[1]

class Rows(Region):
     hover_on_links_locator = (By.CLASS_NAME, "row-title")

     # selects name, description, slug and count from the table
     _taxonomy_name_locator = (By.CLASS_NAME, "row-title")
     _taxonomy_description_locator = (By.CLASS_NAME, "description")
     _taxonomy_slug_locator = (By.CSS_SELECTOR, "[data-colname='Slug']")
     _taxonomy_count_locator = (By.CSS_SELECTOR, "[data-colname='Count']")

     _edit_link_locator = (By.CLASS_NAME, "edit")
     _quick_edit_link_locator = (By.CSS_SELECTOR, "[aria-label*='Quick edit']")
     _delete_link_locator = (By.CSS_SELECTOR, "[aria-label*='Delete']")
     _view_link_locator = (By.CSS_SELECTOR, "[aria-label*='View']")

     _checkbox_locator = (By.CLASS_NAME, "check-column")

     @property
     def hover_on_links(self):
         element = self.find_element(*self.hover_on_links_locator)
         ActionChains(self.driver).move_to_element(element).perform()
         return element

     @property
     def taxonomy_name(self):
         return self.find_element(*self._taxonomy_name_locator)

     @property
     def taxonomy_description(self):
         return self.find_element(*self._taxonomy_description_locator)

     @property
     def taxonomy_slug(self):
         return self.find_element(*self._taxonomy_slug_locator)

     @property
     def taxonomy_count(self):
         return self.find_element(*self._taxonomy_count_locator)

     @property
     def edit_link_click(self):
         return self.find_element(*self._edit_link_locator)

     @property
     def quick_edit_link_click(self):
         return self.find_element(*self._quick_edit_link_locator)

     @property
     def delete_link_click(self):
         return self.find_element(*self._delete_link_locator)

     @property
     def view_link_click(self):
         return self.find_element(*self._view_link_locator)

     @property
     def _select_checkbox(self):
         return self.find_elements(*self._checkbox_locator)
