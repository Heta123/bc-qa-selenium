from selenium.webdriver.common.by import By
from pages.web.wpadmin.v3.create_a_new_card.new_card_base import NewCardBasePage
from selenium.common.exceptions import NoSuchElementException


class CreateNewCatalogCommentCard(NewCardBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _catalog_comment_url_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-url']")
    _grab_comment_info_button_locator = (By.CSS_SELECTOR, "button#js-get-comment-data")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def catalog_comment_url(self):
        return self.find_element(*self._catalog_comment_url_locator)

    @property
    def grab_comment_info(self):
        return self.find_element(*self._grab_comment_info_button_locator)
