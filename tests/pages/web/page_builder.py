from pages.web.staff_base import StaffBasePage
from pages.web.components.page_builder.page_builder import PageBuilderHeader, BuilderPanel
from pages.web.components.page_builder.page_builder_modules import *
from pages.web.components.user_facing_modules import Modules
from utils.selenium_helpers import click
from selenium.webdriver.common.action_chains import ActionChains


class PageBuilderPage(StaffBasePage):

    @property
    def page_builder(self):
        return PageBuilderHeader(self)

    @property
    def builder_panel(self):
        return BuilderPanel(self)

    @property
    def user_facing_modules(self):
        return Modules(self)

    def add_single_card(self, content_type, title):
        if not self.builder_panel.is_panel_visible:
            self.page_builder.add_content.click()
        self.builder_panel.modules_tab.click()
        ActionChains(self.driver).drag_and_drop(self.builder_panel.single_card, self.page_builder.body).perform()
        single_card = PageBuilderSingleCard(self)
        click(single_card.advanced_tab)
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Single Card - " + content_type)
        single_card.edit_content_link.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type(content_type)
        single_card.edit_card.choose_card.click()
        if content_type == "Twitter":
            single_card.edit_card.choose_card_search_input.send_keys(title)
            single_card.edit_card.select_card_by_index(0).click()
        else:
            single_card.edit_card.select_card_by_title(title).click()
        single_card.edit_card.save.click()
        single_card.save()
